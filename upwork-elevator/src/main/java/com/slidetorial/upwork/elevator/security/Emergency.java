/**
 *
 */
package com.slidetorial.upwork.elevator.security;

/**
 * Represents an emergency.
 *
 * @author goobar
 *
 */
public enum Emergency
{
	/**
	 *
	 */
	BLACKOUT,
	/**
	* 
	*/
	FIRE
}
