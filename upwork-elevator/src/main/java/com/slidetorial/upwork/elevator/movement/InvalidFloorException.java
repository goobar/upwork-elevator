/**
 *
 */
package com.slidetorial.upwork.elevator.movement;

import com.slidetorial.upwork.elevator.ElevatorException;

/**
 * @author goobar
 *
 */
@SuppressWarnings("javadoc")
public class InvalidFloorException extends ElevatorException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public InvalidFloorException(String message)
	{
		super(message);
	}

	public InvalidFloorException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
