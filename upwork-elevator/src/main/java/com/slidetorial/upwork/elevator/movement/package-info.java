/**
 *
 */
/**
 * Elevator movement context. This is the main context of this demo project.
 *
 * @author goobar
 *
 */
package com.slidetorial.upwork.elevator.movement;