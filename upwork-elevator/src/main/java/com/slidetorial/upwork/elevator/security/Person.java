/**
 *
 */
package com.slidetorial.upwork.elevator.security;

/**
 * Models a person in the security context.
 *
 * @author goobar
 *
 */
public interface Person
{
	/**
	 * @return the weight of this person
	 */
	public double weight();
}
