/**
 *
 */
package com.slidetorial.upwork.elevator;

/**
 * @author goobar
 *
 */
@SuppressWarnings("javadoc")
public class ElevatorException extends RuntimeException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ElevatorException(String message)
	{
		super(message);
	}

	public ElevatorException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
