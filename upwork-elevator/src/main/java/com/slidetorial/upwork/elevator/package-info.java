/**
 *
 */
/**
 * The project is a draft proposition of elevator domain model. Possibly, there
 * can be multiple contexts in which "elevator" term can be used quite
 * differently. In this project we focus mainly on the movement context.
 * However, also an emergency "demo" context is included, which is something
 * that's quite obvious and should definitely be included as part of some other
 * module. Naturally, the real application would require a lot of knowledge
 * crunching session with domain experts (technicians, building owners etc)
 * before it becomes useful. Here you can see just an initial draft of the very
 * first design.
 *
 * @author goobar
 *
 */
package com.slidetorial.upwork.elevator;