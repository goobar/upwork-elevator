/**
 *
 */
package com.slidetorial.upwork.elevator.impl;

import java.util.ArrayList;
import java.util.List;
import com.slidetorial.upwork.elevator.ElevatorException;
import com.slidetorial.upwork.elevator.movement.CallRequest;
import com.slidetorial.upwork.elevator.movement.Elevator;
import com.slidetorial.upwork.elevator.movement.Floor;
import com.slidetorial.upwork.elevator.movement.FloorChangedListener;
import com.slidetorial.upwork.elevator.movement.InvalidFloorException;
import com.slidetorial.upwork.elevator.movement.MoveScheduler;
import com.slidetorial.upwork.elevator.movement.MovementPlan;

/**
 * A "demo" (obviously not finished and not tested) how a movement context
 * elevator might look like. This implementation uses {@link MoveScheduler} to
 * find the optimal movement plan to handle {@link CallRequest}s. See the
 * {@link #call(CallRequest)} method body to understand the basic concept behind
 * this.
 *
 * @author goobar
 *
 */
public class MoveSchedulerElevator implements Elevator
{
	private final List<CallRequest> callRequests;

	private final MoveScheduler moveScheduler;

	/**
	 * @param moveScheduler
	 *                the scheduler strategy that will be used to determine
	 *                the optimal movement plan
	 */
	public MoveSchedulerElevator(MoveScheduler moveScheduler)
	{
		this.moveScheduler = moveScheduler;
		callRequests = new ArrayList<>();
	}

	/* (non-Javadoc)
	 * @see com.slidetorial.upwork.elevator.movement.Elevator#bottomFloor()
	 */
	@Override
	public Floor bottomFloor()
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/* (non-Javadoc)
	 * @see com.slidetorial.upwork.elevator.movement.Elevator#call(com.slidetorial.upwork.elevator.movement.CallRequest)
	 */
	@Override
	public void call(CallRequest request) throws NullPointerException,
		InvalidFloorException, ElevatorException
	{
		// here the implementation might look similar to this
		callRequests.add(request);
		MovementPlan plan = moveScheduler.schedule(callRequests);
		// execute moves the elevator up or down and decides where it
		// should stop
		execute(plan);

		throw new UnsupportedOperationException("Not yet implemented");
	}

	/* (non-Javadoc)
	 * @see com.slidetorial.upwork.elevator.movement.Elevator#floor()
	 */
	@Override
	public Floor floor()
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/* (non-Javadoc)
	 * @see com.slidetorial.upwork.elevator.movement.Elevator#move(com.slidetorial.upwork.elevator.movement.Floor)
	 */
	@Override
	public void move(Floor floor) throws NullPointerException,
		InvalidFloorException, ElevatorException
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/* (non-Javadoc)
	 * @see com.slidetorial.upwork.elevator.movement.Elevator#registerFloorChangedListener(com.slidetorial.upwork.elevator.movement.FloorChangedListener)
	 */
	@Override
	public void registerFloorChangedListener(FloorChangedListener listener)
		throws NullPointerException, ElevatorException
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/* (non-Javadoc)
	 * @see com.slidetorial.upwork.elevator.movement.Elevator#topFloor()
	 */
	@Override
	public Floor topFloor()
	{
		throw new UnsupportedOperationException("Not yet implemented");
	}

	/**
	 * @param plan
	 */
	private void execute(MovementPlan plan)
	{
		// Execute the plan
		// 1. Move to the next floor
		// 2. Remove the call request after its "consumed"
		// 3. Need to take care about possible synchronization issues
		throw new UnsupportedOperationException("Not yet implemented");
	}

}
