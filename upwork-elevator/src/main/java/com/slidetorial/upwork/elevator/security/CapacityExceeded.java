/**
 *
 */
package com.slidetorial.upwork.elevator.security;

import com.slidetorial.upwork.elevator.ElevatorException;

/**
 * @author goobar
 *
 */
@SuppressWarnings("javadoc")
public class CapacityExceeded extends ElevatorException
{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public CapacityExceeded(String message)
	{
		super(message);
	}

	public CapacityExceeded(String message, Throwable cause)
	{
		super(message, cause);
	}

}
