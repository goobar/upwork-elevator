/**
 *
 */
package com.slidetorial.upwork.elevator.security;

/**
 * A component to handle emergency situations.
 *
 * @author goobar
 *
 */
public interface EmergencyHandler
{
	/**
	 * Handles an emergency situation.
	 *
	 * @param emergency
	 *                an emergency
	 * @param elevator
	 *                the elevator at which emergency occurred
	 * @throws NullPointerException
	 *                 if any argument is null
	 */
	void handle(Emergency emergency, Elevator elevator)
		throws NullPointerException;
}
