/**
 *
 */
/**
 * A simple "demo" context which uses the "elevator" term in the security
 * context. This package is included here only to show that something as simple
 * as elevator is in fact quite sophisticated. Most probably, in the real
 * application there would be a lot of different contexts, each using "elevator"
 * for its own needs and with completely different meaning.
 *
 * @author goobar
 *
 */
package com.slidetorial.upwork.elevator.security;