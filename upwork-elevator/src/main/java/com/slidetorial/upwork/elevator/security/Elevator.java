/**
 *
 */
package com.slidetorial.upwork.elevator.security;

import java.util.Collection;
import com.slidetorial.upwork.elevator.ElevatorException;

/**
 * A simple "demo" interface showing the security side of the elevator.
 *
 * @author goobar
 *
 */
public interface Elevator
{

	/**
	 * Closes the door.
	 *
	 * @throws ElevatorException
	 *                 if fails
	 */
	void closeDoor() throws ElevatorException;

	/**
	 * "Adds" a person to the elevator.
	 *
	 * @param person
	 *                a person
	 * @throws NullPointerException
	 *                 if any argument is null
	 * @throws CapacityExceeded
	 *                 if weight/people capacity is exceeded
	 * @throws ElevatorException
	 *                 if fails
	 */
	void enter(Person person) throws NullPointerException, CapacityExceeded,
		ElevatorException;

	/**
	 * A check which evaluates all security rules and returns the final
	 * result.
	 *
	 * @return {@code true} if all security checks passed and an elevator is
	 *         ready to go
	 */
	boolean isSafeToGo();

	/**
	 * Opens the door.
	 *
	 * @throws ElevatorException
	 *                 if fails
	 */
	void openDoor() throws ElevatorException;

	/**
	 * @return people which are currently in the elevator
	 */
	Collection<Person> people();

	/**
	 * Registers an emergency handler.
	 *
	 * @param handler
	 * @throws NullPointerException
	 * @throws ElevatorException
	 */
	void registerEmergencyHandler(EmergencyHandler handler)
		throws NullPointerException, ElevatorException;
}
