/**
 *
 */
package com.slidetorial.upwork.elevator.movement;

import com.slidetorial.upwork.elevator.ElevatorException;

/**
 * An interface to control the elevator movement.
 *
 * @author goobar
 *
 */
public interface Elevator
{
	/**
	 * @return the bottom floor
	 */
	Floor bottomFloor();

	/**
	 * Calls the elevator to move from one floor to another. This method is
	 * intended to be invoked on user's request. The reason why this method
	 * uses a {@link CallRequest} instead of a simple destination floor, is
	 * that a user can request the destination floor immediately (using the
	 * door panel), instead of the panel inside the elevator. This approach
	 * allows us to schedule the elevator movement more effectively. The
	 * movement plan depends on the concrete implementation. For example,
	 * the most naive implementation might just call {@link #move(Floor)}
	 * method directly consuming the requests using simple FIFO queue.
	 * However, the production-ready implementation will most probably try
	 * minimize the distance traveled.
	 *
	 * @param request
	 *                a move request
	 * @throws NullPointerException
	 *                 if any argument is null
	 * @throws InvalidFloorException
	 *                 if move request is invalid
	 * @throws ElevatorException
	 *                 if fails
	 */
	void call(CallRequest request) throws NullPointerException,
		InvalidFloorException, ElevatorException;

	/**
	 * Returns the current floor. <b>Note:</b> If elevator is in motion,
	 * then consecutive calls to this method might return different results
	 * each time. if you want to receive notifications when the elevator
	 * moves up or down, you are invited to use
	 * {@link #registerFloorChangedListener(FloorChangedListener)}.
	 *
	 * @return the floor on which the elevator currently is
	 */
	Floor floor();

	/**
	 * Immediately moves the elevator to the given floor. All user requests
	 * are immediately ignored and discarded. Generally, this method should
	 * be called in emergency situations directly by the elevator operator.
	 *
	 * @param floor
	 *                a floor position, can be any value between
	 *                {@link #bottomFloor()} and {@link #topFloor()}
	 *                (inclusive)
	 * @throws NullPointerException
	 *                 if any argument is null
	 * @throws InvalidFloorException
	 *                 if given floor is invalid
	 * @throws ElevatorException
	 *                 if fails
	 */
	void move(Floor floor) throws NullPointerException,
		InvalidFloorException, ElevatorException;

	/**
	 * Registers a {@link FloorChangedListener} which will be notified when
	 * the elevator moved up or down.
	 *
	 * @param listener
	 *                a listener
	 * @throws NullPointerException
	 *                 if any argument is null
	 * @throws ElevatorException
	 *                 if fails
	 */
	void registerFloorChangedListener(FloorChangedListener listener)
		throws NullPointerException, ElevatorException;

	/**
	 * @return the top floor
	 */
	Floor topFloor();
}
