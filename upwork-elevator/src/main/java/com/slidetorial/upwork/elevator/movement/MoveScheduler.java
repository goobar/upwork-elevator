/**
 *
 */
package com.slidetorial.upwork.elevator.movement;

import java.util.List;

/**
 * The central strategy interface to schedule an optimal elevator movement plan.
 *
 * @author goobar
 *
 */
public interface MoveScheduler
{
	/**
	 * Schedules an optimal movement plan for call requests.
	 *
	 * @param requests
	 *                a call request
	 * @return the optimal plan
	 * @throws NullPointerException
	 *                 if any argument is null
	 */
	MovementPlan schedule(List<CallRequest> requests)
		throws NullPointerException;
}
