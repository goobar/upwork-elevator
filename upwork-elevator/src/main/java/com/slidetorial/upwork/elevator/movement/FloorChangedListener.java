/**
 *
 */
package com.slidetorial.upwork.elevator.movement;

/**
 * A component to handle floor change events.
 * 
 * @author goobar
 *
 */
public interface FloorChangedListener
{
	/**
	 * Called when floor is changed.
	 *
	 * @param previousFloor
	 *                a previous floor
	 * @param currentFloor
	 *                a current floor
	 * @throws NullPointerException
	 *                 if any argument is null
	 */
	void floorChanged(Floor previousFloor, Floor currentFloor)
		throws NullPointerException;
}
