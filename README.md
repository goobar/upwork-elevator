# README #

The project is a draft proposition of elevator domain model. Possibly, there can be multiple contexts in which "elevator" term can be used quite differently. In this project we focus mainly on the movement context. However, also an emergency "demo" context is included, which is something that's quite obvious and should definitely be included as part of some other module. Naturally, the real application would require a lot of knowledge crunching session with domain experts (technicians, building owners etc) before it becomes useful. Here you can see just an initial draft of the very first design.

### Movement context ###

This is the main context of this demo. There are a lot of javadocs and most of the components are expressed through interfaces. There are two main methods of the Elevator interface: move and call.

The class diagram in png format is included in src/main/resources.

### Move method ###

This method mmediately moves the elevator to the given floor. All user requests are immediately ignored and discarded. Generally, this method should be called in emergency situations directly by the elevator operator.

### Call method ###

This is more complicated. The method calls the elevator to move from one floor to another. This method is intended to be invoked on user's request. The reason why this method uses a CallRequest instead of a simple destination floor, is that a user can request the destination floor immediately (using the door panel), instead of the panel inside the elevator. This approach allows us to schedule the elevator movement more effectively. The movement plan depends on the concrete implementation. For example, the most naive implementation might just call move(Floor) method directly consuming the requests using simple FIFO queue. However, the production-ready implementation will most probably try minimize the distance traveled.

### Move Scheduler ###

This is the central strategy interface to schedule an optimal elevator movement plan when there are multiple user calls. Basically, the idea behind the design is to decouple the "optimal movement plan" strategy from other concerns. This allows us later to easily replace the way we handle user calls without a need to modify the existing code. Also, this design is easy to comprehend as it expresses the intent nicely.

An exemplary implementation might follow the natural approach: don't stop on floors where users want to go up, but the elevator is already going down or vice versa. Generally speaking, the optimal solution takes into consideration the following things: 1. Serve every request (obvious), 2. Minimize the distance, 3. Minimize the number of stops, 4. Don't take people if it's not necessary (e.g. when a user want to go top but the elevator is prioritized to go down).

### The example - MoveSchedulerElevator ###

This example shows how an implementation using MoveScheduler might look like. The call method simply handles the call requests, passes them to the MoveScheduler, receives the optimal plan and then simply executes it.